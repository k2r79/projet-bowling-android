package com.bowling.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.bowling.R;

import java.util.List;

/**
 * Created by kimsavinfo on 10/06/14.
 */
public class ListePistesAdapter extends ArrayAdapter<String>
{
    Context context;

    public ListePistesAdapter(Context context, int resourceId, List<String> items)
    {
        super(context, resourceId, items);
        this.context = context;
    }

    // Classe privée pour manipuler la View
    private class ViewHolder
    {
        TextView nom;
        TextView disponibilite;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder = null;
        String piste = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = mInflater.inflate(R.layout.piste_cell, null);
            holder = new ViewHolder();
            holder.nom = (TextView) convertView.findViewById(R.id.piste_nom);
            holder.disponibilite = (TextView) convertView.findViewById(R.id.piste_indication);

            convertView.setTag(holder);
        }else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.nom.setText(piste);
        holder.disponibilite.setText("30:00");

        return convertView;
    }

}
