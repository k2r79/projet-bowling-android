package com.bowling.joueur;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.bowling.R;

/**
 * Created by kimsavinfo on 04/06/14.
 */
public class JoueurFragment extends Fragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.joueur_infos, container, false);

        EditText nomView = (EditText) view.findViewById(R.id.client_nom);
        nomView.setText("DURAND");

        EditText prenomView = (EditText) view.findViewById(R.id.client_prenom);
        prenomView.setText("Toto");

        EditText emailView = (EditText) view.findViewById(R.id.client_email);
        emailView.setText("monadresse@gmail.com");

        return view;
    }
}


