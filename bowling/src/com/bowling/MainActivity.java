package com.bowling;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import com.bowling.joueur.JoueurFragment;
import com.bowling.parametres.ParametresActivity;
import com.bowling.pistes.ListePistesFragment;
import com.example.bowling.R;

public class MainActivity extends FragmentActivity implements ActionBar.TabListener
{

    private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
    private ActionBar actionBar;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_main);

        // Paramétrer l'actionBar
        actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // 1 section = 1 bar
        actionBar.addTab(actionBar.newTab().setText(R.string.tab_1).setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText(R.string.tab_2).setTabListener(this));
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {
        // Associer 1 barre à 1 fragment
        if (tab.getPosition() == 0) {
            ListePistesFragment simpleListFragment = new ListePistesFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.container, simpleListFragment).commit();
        }
        else if (tab.getPosition() == 1) {

            JoueurFragment joueurFragment = new JoueurFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.container, joueurFragment).commit();
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState)
    {
        if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM))
        {
            actionBar.setSelectedNavigationItem(savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar().getSelectedNavigationIndex());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == R.id.menu_settings) {
            Intent preferencesIntent = new Intent(MainActivity.this, ParametresActivity.class);
            startActivity(preferencesIntent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {
    }
}
