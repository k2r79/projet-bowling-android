package com.bowling.parametres;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import com.example.bowling.R;

public class ParametresActivity extends PreferenceActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
    }
}
