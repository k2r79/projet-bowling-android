package com.bowling.pistes;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.bowling.adapter.ListePistesAdapter;
import com.example.bowling.R;

import java.util.ArrayList;
import java.util.List;

public class ListePistesFragment extends ListFragment
{
    ListView pistesLisView;
    List<String> lesPistes;

	public ListePistesFragment()
    {
        lesPistes = new ArrayList<String>();
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        ListePistesAdapter listePistesAdapter = new ListePistesAdapter(getActivity(), R.layout.liste_pistes, lesPistes);
        setListAdapter(listePistesAdapter);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.liste_pistes, container, false);

        // Peupler la liste
        pistesLisView = (ListView)view.findViewById(android.R.id.list);
        pistesLisView.setOnItemClickListener(new ItemClick());

        new RecupererListePistesAsyncTache(this).execute();

        return view;
	}
	
	@Override
	public void onListItemClick(ListView list, View v, int position, long id)
    {
		
		Toast.makeText(getActivity(), getListView().getItemAtPosition(position).toString(), Toast.LENGTH_LONG).show();
	}

    public ListView getListePitesView()
    {
        return pistesLisView;
    }

    private class ItemClick implements android.widget.AdapterView.OnItemClickListener
    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
        {
            // TODO : Action quand click
        }
    }
}
