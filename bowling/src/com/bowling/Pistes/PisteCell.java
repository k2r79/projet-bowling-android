package com.bowling.pistes;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bowling.R;

/**
 * Created by kimsavinfo on 10/06/14.
 */
public class PisteCell extends Fragment
{
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.piste_cell, container, false);

        /*
        Bundle extras = getIntent().getExtras();



        if (extras != null)
        {
            String nomPiste = extras.getString("nomPiste");

            FragmentPistes detailFragment = (FragmentPistes)getSupportFragmentManager().findFragmentById(R.id.liste_pistes);
            detailFragment.setText(titre, description, url);
        }
        */

        return view;
    }
}
