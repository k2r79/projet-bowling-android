package com.bowling.pistes;

import android.os.AsyncTask;
import android.widget.Toast;

import com.bowling.adapter.ListePistesAdapter;
import com.example.bowling.R;

import java.util.ArrayList;
import java.util.List;

/**
* Created by kimsavinfo on 10/06/14.
*/
class RecupererListePistesAsyncTache extends AsyncTask<String, Void, List<String>>
{
    private ListePistesFragment listePistesFragment;

    public RecupererListePistesAsyncTache(ListePistesFragment p_listPistesFragment) {
        listePistesFragment = p_listPistesFragment;
    }

    // TODO :  Méthode à adapter en fonction des données envoyées par le bot.
    // Exemple avec des strings pour faciliter la compréhension
    @Override
    protected List<String> doInBackground(String... params)
    {
        List<String> pistes = new ArrayList<String>();

        pistes.add("Piste alpha");
        pistes.add("Piste beta");
        pistes.add("Piste gamma");

        return pistes;
    }
    @Override
    protected void onPostExecute(List<String> pistes)
    {
        if (pistes == null)
        {
            Toast.makeText(listePistesFragment.getActivity().getBaseContext(), "Aucune piste disponible", Toast.LENGTH_LONG);
        }
        else
        {
            for (String piste : pistes)
            {
                ListePistesAdapter adapteur = new ListePistesAdapter(listePistesFragment.getActivity().getBaseContext(), R.layout.piste_cell, pistes);
                listePistesFragment.getListePitesView().setAdapter(adapteur);
            }
        }
    }
}
